# README #

Author: Jacob Brown, jbrown14@uoregon.edu 
Project 2, 10/22/18, for CIS 322, Fall 2018

Description: A simple Flask server that utilizes Docker. Implemented with similar error-checking code as in Project 1.

Instructions:
In the web folder containing the Dockerfile, use the following commands to build and run this program respectively:


```
docker build -t proj2-pageserver .
```

```
docker run -d -p 5000:5000 proj2-pageserver
```


Then, go to localhost:5000 in a web browser. localhost:5000/trivia.html will return an HTML page, localhost:5000/trivia.css will return a .css file, and any other URL will return an HTML error page (either File not found! or File is forbidden!"

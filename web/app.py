# app.py
#
# Edited by Jacob Brown, 10/22/18
# for Project 2, CIS 322, Fall 2018
#
# Returns either an existing html or error-handling
# html files if a file doesn't exit or is forbidden

from flask import Flask, render_template

app = Flask(__name__)

# Catch-all routes for URLs
# (Idea from flask.pocoo.org/snippets/57/)
@app.route('/', defaults={'pname': ''})
@app.route("/<path:pname>")
def router(pname):

    # Check path ending
    if pname.endswith(".html") or pname.endswith(".css"):

        # Check invalid substrings
        if "//" not in pname and "~" not in pname and ".." not in pname:
            
            # Try/catch to make sure the path submitted 
            # actually exists, and if it does we submit 
            # it with the 200 HTML code (which it does 
            # by default anyway, but hey)
            try:
                return render_template(pname), 200
            except:
                return error_404("")
           
        else:

            # Returns a file forbidden if the path has
            # the invalid substrings
            return error_403("")

    # Returns a file not found by default
    return error_404("")

@app.errorhandler(403)
def error_403(error):
    return render_template("403.html"), 403

@app.errorhandler(404)
def error_404(error):
    return render_template("404.html"), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
